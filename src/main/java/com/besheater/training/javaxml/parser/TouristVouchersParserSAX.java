package com.besheater.training.javaxml.parser;

import com.besheater.training.javaxml.entity.Cost;
import com.besheater.training.javaxml.entity.Destination;
import com.besheater.training.javaxml.entity.Duration;
import com.besheater.training.javaxml.entity.TouristVoucher;
import com.besheater.training.javaxml.entity.hotel.Facility;
import com.besheater.training.javaxml.entity.hotel.Hotel;
import com.besheater.training.javaxml.entity.hotel.Meal;
import com.besheater.training.javaxml.entity.hotel.Service;
import com.besheater.training.javaxml.entity.hotel.room.Bed;
import com.besheater.training.javaxml.entity.hotel.room.BedType;
import com.besheater.training.javaxml.entity.hotel.room.Room;
import com.besheater.training.javaxml.entity.hotel.room.RoomFacility;
import com.besheater.training.javaxml.entity.transport.Baggage;
import com.besheater.training.javaxml.entity.transport.LengthUnit;
import com.besheater.training.javaxml.entity.transport.MassUnit;
import com.besheater.training.javaxml.entity.transport.Transport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.net.URL;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.stream.Stream;

public class TouristVouchersParserSAX extends DefaultHandler implements TouristVouchersParser {
    private static final Logger LOG = LogManager.getLogger();

    private String elementValue;

    private List<TouristVoucher> touristVouchers = new ArrayList<>();
    private TouristVoucher touristVoucher;
    private Destination destination;
    private Duration duration;
    private Transport transport;
    private Hotel hotel;
    private Cost cost;

    private Baggage baggage;
    private Meal meal;
    private Service service;
    private Facility facility;
    private Room room;
    private Bed bed;
    private RoomFacility roomFacility;

    @Override
    public Stream<TouristVoucher> parse(URL xml) throws ParseException {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            saxParser.parse(xml.openStream(), this);
            return touristVouchers.stream();
        } catch (Exception ex) {
            LOG.error("Parsing failed", ex);
            throw new ParseException("Parsing failed", ex);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        elementValue = new String(ch, start, length);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        switch (qName) {
            case "touristvoucher":
                clearAllVariables();
                long id = Long.parseLong(attributes.getValue("id"));
                String type = attributes.getValue("type");
                touristVoucher = new TouristVoucher(id, type);
                break;
            case "destination":
                destination = new Destination();
                break;
            case "country":
                break;
            case "province":
                break;
            case "city":
                break;
            case "duration":
                duration = new Duration();
                break;
            case "days":
                break;
            case "nights":
                break;
            case "transport":
                transport = new Transport();
                transport.setType(attributes.getValue("type"));
                break;
            case "class":
                break;
            case "baggage":
                baggage = new Baggage();
                baggage.setType(attributes.getValue("type"));
                break;
            case "size":
                baggage.setLengthUnit(LengthUnit.parse(attributes.getValue("unit")));
                break;
            case "total":
                break;
            case "dimensions":
                break;
            case "weight":
                baggage.setMassUnit(MassUnit.parse(attributes.getValue("unit")));
                break;
            case "hotel":
                if (cost == null) {
                    // <hotel> inside <touristvoucher>
                    hotel = new Hotel();
                    hotel.setId(attributes.getValue("id"));
                    hotel.setStarRating(Integer.parseInt(attributes.getValue("starrating")));
                } else {
                    // <hotel> inside <cost>
                }
                break;
            case "meal":
                meal = new Meal();
                break;
            case "boardbasis":
                break;
            case "specialrequest":
                break;
            case "services":
                break;
            case "service":
                service = new Service();
                service.setType(attributes.getValue("type"));
                break;
            case "facilities":
                break;
            case "facility":
                if (room == null) {
                    // <facility> inside <hotel>
                    facility = new Facility();
                    facility.setType(attributes.getValue("type"));
                } else {
                    // <facility> inside <room>
                    roomFacility = new RoomFacility();
                    String quantity = attributes.getValue("quantity");
                    if (quantity != null) {
                        roomFacility.setQuantity(Integer.parseInt(quantity));
                    }
                }
                break;
            case "room":
                room = new Room();
                break;
            case "sleeps":
                room.setSleeps(Integer.parseInt(attributes.getValue("quantity")));
                break;
            case "beds":
                break;
            case "bed":
                bed = new Bed();
                bed.setType(BedType.parse(attributes.getValue("type")));
                bed.setQuantity(Integer.parseInt(attributes.getValue("quantity")));
                break;
            case "cost":
                cost = new Cost();
                cost.setCurrency(Currency.getInstance(attributes.getValue("currency")));
                break;
            case "travel":
                break;
            case "activities":
                break;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        switch (qName) {
            case "touristvoucher":
                touristVouchers.add(touristVoucher);
                break;
            case "destination":
                touristVoucher.setDestination(destination);
                break;
            case "country":
                destination.setCountry(elementValue);
                break;
            case "province":
                destination.setProvince(elementValue);
                break;
            case "city":
                destination.setCity(elementValue);
                break;
            case "duration":
                touristVoucher.setDuration(duration);
                break;
            case "days":
                duration.setDays(Integer.parseInt(elementValue));
                break;
            case "nights":
                duration.setNights(Integer.parseInt(elementValue));
                break;
            case "transport":
                touristVoucher.setTransport(transport);
                break;
            case "class":
                transport.setTravelClass(elementValue);
                break;
            case "baggage":
                transport.addBaggage(baggage);
                break;
            case "size":
                break;
            case "total":
                baggage.setTotalDimension(Integer.parseInt(elementValue));
                break;
            case "dimensions":
                baggage.setDimensions(elementValue);
                break;
            case "weight":
                baggage.setWeight(Integer.parseInt(elementValue));
                break;
            case "hotel":
                if (cost == null) {
                    // <hotel> inside <touristvoucher>
                    touristVoucher.setHotel(hotel);
                } else {
                    // <hotel> inside <cost>
                    cost.setHotelCost(Double.parseDouble(elementValue));
                }
                break;
            case "meal":
                hotel.setMeal(meal);
                break;
            case "boardbasis":
                meal.setBoardBasis(elementValue);
                break;
            case "specialrequest":
                meal.setSpecialRequest(elementValue);
                break;
            case "services":
                break;
            case "service":
                service.setDescription(elementValue);
                hotel.addService(service);
                break;
            case "facilities":
                break;
            case "facility":
                if (room == null) {
                    // <facility> inside <hotel>
                    facility.setDescription(elementValue);
                    hotel.addFacility(facility);
                } else {
                    // <facility> inside <room>
                    roomFacility.setDescription(elementValue);
                    room.addFacility(roomFacility);
                }
                break;
            case "room":
                hotel.setRoom(room);
                break;
            case "sleeps":
                break;
            case "beds":
                break;
            case "bed":
                room.addBed(bed);
                break;
            case "cost":
                touristVoucher.setCost(cost);
                break;
            case "travel":
                cost.setTravelCost(Double.parseDouble(elementValue));
                break;
            case "activities":
                cost.setActivitiesCost(Double.parseDouble(elementValue));
                break;
        }
    }

    private void clearAllVariables() {
        touristVoucher = null;
        destination = null;
        duration = null;
        transport = null;
        hotel = null;
        cost = null;

        baggage = null;
        meal = null;
        service = null;
        facility = null;
        room = null;
        bed = null;
        roomFacility = null;
    }
}