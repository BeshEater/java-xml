package com.besheater.training.javaxml.parser;

import com.besheater.training.javaxml.entity.TouristVoucher;

import java.net.URL;
import java.util.stream.Stream;

public interface TouristVouchersParser {

    Stream<TouristVoucher> parse(URL xml) throws ParseException;
}