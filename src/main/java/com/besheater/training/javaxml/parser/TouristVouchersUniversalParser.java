package com.besheater.training.javaxml.parser;

import com.besheater.training.javaxml.entity.TouristVoucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.net.URL;
import java.util.stream.Stream;

public class TouristVouchersUniversalParser {
    private static final Logger LOG = LogManager.getLogger();

    private XMLparserType parserType;
    private URL xml;
    private URL xsd;

    private TouristVouchersUniversalParser(Builder builder) {
        this.parserType = builder.parserType;
        this.xml = builder.xml;
        this.xsd = builder.xsd;
        LOG.debug("New instance created");
    }

    private void validate() throws IOException, SAXException {
        Source xmlSource = new StreamSource(xml.openStream());
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            Schema schema = schemaFactory.newSchema(xsd);
            Validator validator = schema.newValidator();
            validator.validate(xmlSource);
            LOG.debug("XML file is valid");
        } catch (SAXException ex) {
            LOG.error("XML file is not valid", ex);
            throw new SAXException("XML file is not valid", ex);
        }
    }

    public Stream<TouristVoucher> parse() throws ParseException {
        try {
            validate();
        } catch (IOException | SAXException ex) {
            throw new ParseException(ex);
        }
        switch (parserType) {
            case DOM:
                return parseUsingDOM();
            case SAX:
                return parseUsingSAX();
            case STAX:
                return parseUsingStAX();
            default:
                return null;
        }
    }

    private Stream<TouristVoucher> parseUsingDOM() throws ParseException {
        return new TouristVouchersParserDOM().parse(xml);
    }

    private Stream<TouristVoucher> parseUsingSAX() throws ParseException {
        return new TouristVouchersParserSAX().parse(xml);
    }

    private Stream<TouristVoucher> parseUsingStAX() throws ParseException {
        return new TouristVouchersParserStAX().parse(xml);
    }

    public static class Builder {
        private XMLparserType parserType;
        private URL xml;
        private URL xsd;

        public Builder setParserType(XMLparserType parserType) {
            this.parserType = parserType;
            return this;
        }

        public Builder setXML(URL xml) {
            this.xml = xml;
            return this;
        }

        public Builder setXSD(URL xsd) {
            this.xsd = xsd;
            return this;
        }

        public TouristVouchersUniversalParser build() {
            return new TouristVouchersUniversalParser(this);
        }
    }
}