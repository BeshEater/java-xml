package com.besheater.training.javaxml.parser;

import com.besheater.training.javaxml.entity.Cost;
import com.besheater.training.javaxml.entity.Destination;
import com.besheater.training.javaxml.entity.Duration;
import com.besheater.training.javaxml.entity.TouristVoucher;
import com.besheater.training.javaxml.entity.hotel.Facility;
import com.besheater.training.javaxml.entity.hotel.Hotel;
import com.besheater.training.javaxml.entity.hotel.Meal;
import com.besheater.training.javaxml.entity.hotel.Service;
import com.besheater.training.javaxml.entity.hotel.room.Bed;
import com.besheater.training.javaxml.entity.hotel.room.BedType;
import com.besheater.training.javaxml.entity.hotel.room.Room;
import com.besheater.training.javaxml.entity.hotel.room.RoomFacility;
import com.besheater.training.javaxml.entity.transport.Baggage;
import com.besheater.training.javaxml.entity.transport.LengthUnit;
import com.besheater.training.javaxml.entity.transport.MassUnit;
import com.besheater.training.javaxml.entity.transport.Transport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.net.URL;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class TouristVouchersParserDOM implements TouristVouchersParser{
    private static final Logger LOG = LogManager.getLogger();

    @Override
    public Stream<TouristVoucher> parse(URL xml) throws ParseException {
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(xml.openStream());
            doc.getDocumentElement().normalize();
            NodeList toursNodes = doc.getElementsByTagName("touristvoucher");
            LOG.debug("<touristvoucher> tags count = {}", toursNodes.getLength());

            return IntStream.range(0, toursNodes.getLength())
                    .mapToObj(i -> (Element) toursNodes.item(i))
                    .map(this::getTouristVoucher);
        } catch (Exception ex) {
            LOG.error("Parsing failed", ex);
            throw new ParseException("Parsing failed", ex);
        }
    }

    private TouristVoucher getTouristVoucher(Element touristVoucherElem) {
        long id = Long.parseLong(touristVoucherElem.getAttribute("id"));
        String type = touristVoucherElem.getAttribute("type");
        Element destination = getElementByTagName(touristVoucherElem, "destination");
        Element duration = getElementByTagName(touristVoucherElem, "duration");
        Element transport = getElementByTagName(touristVoucherElem, "transport");
        Element hotel = getElementByTagName(touristVoucherElem, "hotel");
        Element cost = getElementByTagName(touristVoucherElem, "cost");

        TouristVoucher touristVoucher = new TouristVoucher(id, type);
        touristVoucher.setDestination(getDestination(destination));
        touristVoucher.setDuration(getDuration(duration));
        touristVoucher.setTransport(getTransport(transport));
        touristVoucher.setHotel(getHotel(hotel));
        touristVoucher.setCost(getCost(cost));
        LOG.debug("Tourist voucher = {}", touristVoucher);
        return touristVoucher;
    }

    private Destination getDestination(Element destination) {
        String country = getElementByTagName(destination, "country").getTextContent();
        String province = getElementByTagName(destination, "province").getTextContent();
        String city = getElementByTagName(destination, "city").getTextContent();

        return new Destination(country, province, city);
    }

    private Duration getDuration(Element duration) {
        int days = Integer.parseInt(getElementByTagName(duration, "days").getTextContent());
        int nights = Integer.parseInt(getElementByTagName(duration, "nights").getTextContent());

        return new Duration(days, nights);
    }

    private Transport getTransport(Element transport) {
        String type = transport.getAttribute("type");
        String travelClass = getElementByTagName(transport, "class").getTextContent();
        List<Baggage> baggageList = getBaggageList(transport);

        return new Transport(type, travelClass, baggageList);
    }

    private List<Baggage> getBaggageList(Element transport) {
        NodeList baggageNodes = transport.getElementsByTagName("baggage");

        List<Baggage> baggageList = new ArrayList<>(baggageNodes.getLength());
        for (int nodeIndex = 0; nodeIndex < baggageNodes.getLength(); nodeIndex++) {
            Element baggageElem = (Element) baggageNodes.item(nodeIndex);
            Element sizeElem = getElementByTagName(baggageElem, "size");
            Element weightElem = getElementByTagName(baggageElem, "weight");

            String type = baggageElem.getAttribute("type");
            LengthUnit lengthUnit = getLengthUnit(sizeElem);
            MassUnit massUnit = getMassUnit(weightElem);
            int weight = getWeight(weightElem);
            Baggage baggage;
            if (isSizeTotal(sizeElem)) {
                int totalDimension = Integer.parseInt(
                        getElementByTagName(sizeElem, "total").getTextContent());
                baggage = new Baggage(type, massUnit, weight, lengthUnit, totalDimension);
            } else {
                String dimensions = getElementByTagName(sizeElem, "dimensions").getTextContent();
                baggage = new Baggage(type, massUnit, weight, lengthUnit, dimensions);
            }
            LOG.debug("Baggage to add = {}", baggage);
            baggageList.add(baggage);
        }
        return baggageList;
    }

    private boolean isSizeTotal(Element sizeElem) {
        return sizeElem.getElementsByTagName("total").getLength() > 0;
    }

    private LengthUnit getLengthUnit(Element sizeElem) {
        String unitString = sizeElem.getAttribute("unit");
        return LengthUnit.parse(unitString);
    }

    private MassUnit getMassUnit(Element weightElem) {
        String unitString = weightElem.getAttribute("unit");
        return MassUnit.parse(unitString);
    }

    private int getWeight(Element weightElem) {
        return Integer.parseInt(weightElem.getTextContent());
    }

    private Hotel getHotel(Element hotel) {
        String id = hotel.getAttribute("id");
        int starRating = Integer.parseInt(hotel.getAttribute("starrating"));
        Meal meal = getMeal(hotel);
        List<Service> services = getServices(hotel);
        List<Facility> facilities = getFacilities(hotel);
        Room room = getRoom(hotel);

        return new Hotel(id, starRating, meal, services, facilities, room);
    }

    private Meal getMeal(Element hotel) {
        Meal meal = new Meal();
        Element mealElem = getElementByTagName(hotel, "meal");
        meal.setBoardBasis(getElementByTagName(mealElem, "boardbasis").getTextContent());
        Element specialRequestElem = getElementByTagName(mealElem, "specialrequest");
        if (specialRequestElem != null) {
            meal.setSpecialRequest(specialRequestElem.getTextContent());
        }
        return meal;
    }

    private List<Service> getServices(Element hotel) {
        Element servicesElem = getElementByTagName(hotel, "services");
        NodeList servicesNodes = servicesElem.getElementsByTagName("service");

        List<Service> services = new ArrayList<>(servicesNodes.getLength());
        for (int nodeIndex = 0; nodeIndex < servicesNodes.getLength(); nodeIndex++) {
            Element serviceElem = (Element) servicesNodes.item(nodeIndex);
            String type = serviceElem.getAttribute("type");
            String description = serviceElem.getTextContent();
            services.add(new Service(type, description));
        }
        return services;
    }

    private List<Facility> getFacilities(Element hotel) {
        Element facilitiesElem = getElementByTagName(hotel, "facilities");
        NodeList facilitiesNodes = facilitiesElem.getElementsByTagName("facility");

        List<Facility> facilities = new ArrayList<>(facilitiesNodes.getLength());
        for (int nodeIndex = 0; nodeIndex < facilitiesNodes.getLength(); nodeIndex++) {
            Element facilityElem = (Element) facilitiesNodes.item(nodeIndex);
            String type = facilityElem.getAttribute("type");
            String description = facilityElem.getTextContent();
            facilities.add(new Facility(type, description));
        }
        return facilities;
    }

    private Room getRoom(Element hotel) {
        Element roomElem = getElementByTagName(hotel, "room");
        int sleeps = getSleeps(roomElem);
        List<Bed> beds = getBeds(roomElem);
        List<RoomFacility> facilities = getRoomFacilities(roomElem);

        return new Room(sleeps, beds, facilities);
    }

    private int getSleeps(Element roomElem) {
        Element sleepsElem = getElementByTagName(roomElem, "sleeps");
        return Integer.parseInt(sleepsElem.getAttribute("quantity"));
    }

    private List<Bed> getBeds(Element roomElem) {
        Element bedsElem = getElementByTagName(roomElem, "beds");
        NodeList bedsNodes = bedsElem.getElementsByTagName("bed");

        List<Bed> beds = new ArrayList<>(bedsNodes.getLength());
        for (int nodeIndex = 0; nodeIndex < bedsNodes.getLength(); nodeIndex++) {
            Element bedElem = (Element) bedsNodes.item(nodeIndex);
            BedType type = BedType.parse(bedElem.getAttribute("type"));
            int quantity = Integer.parseInt(bedElem.getAttribute("quantity"));
            beds.add(new Bed(type, quantity));
        }
        return beds;
    }

    private List<RoomFacility> getRoomFacilities(Element roomElem) {
        Element facilitiesElem = getElementByTagName(roomElem, "facilities");
        NodeList facilitiesNodes = facilitiesElem.getElementsByTagName("facility");

        List<RoomFacility> facilities = new ArrayList<>(facilitiesNodes.getLength());
        for (int nodeIndex = 0; nodeIndex < facilitiesNodes.getLength(); nodeIndex++) {
            Element facilityElem = (Element) facilitiesNodes.item(nodeIndex);
            String description = facilityElem.getTextContent();
            RoomFacility facility;
            if (facilityElem.hasAttribute("quantity")) {
                int quantity = Integer.parseInt(facilityElem.getAttribute("quantity"));
                facility = new RoomFacility(quantity, description);
            } else {
                facility = new RoomFacility(description);
            }
            facilities.add(facility);
        }
        return facilities;
    }

    private Cost getCost(Element cost) {
        Currency currency = Currency.getInstance(cost.getAttribute("currency"));
        double travelCost = Double.parseDouble(
                getElementByTagName(cost, "travel").getTextContent());
        double hotelCost = Double.parseDouble(
                getElementByTagName(cost, "hotel").getTextContent());
        double activitiesCost = Double.parseDouble(
                getElementByTagName(cost, "activities").getTextContent());

        return new Cost(currency, travelCost, hotelCost, activitiesCost);
    }

    private Element getElementByTagName(Element element, String tagName) {
        return (Element) element.getElementsByTagName(tagName).item(0);
    }
}