package com.besheater.training.javaxml.parser;

public enum XMLparserType {
    DOM, SAX, STAX
}