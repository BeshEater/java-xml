package com.besheater.training.javaxml.parser;

import com.besheater.training.javaxml.entity.Cost;
import com.besheater.training.javaxml.entity.Destination;
import com.besheater.training.javaxml.entity.Duration;
import com.besheater.training.javaxml.entity.TouristVoucher;
import com.besheater.training.javaxml.entity.hotel.Facility;
import com.besheater.training.javaxml.entity.hotel.Hotel;
import com.besheater.training.javaxml.entity.hotel.Meal;
import com.besheater.training.javaxml.entity.hotel.Service;
import com.besheater.training.javaxml.entity.hotel.room.Bed;
import com.besheater.training.javaxml.entity.hotel.room.BedType;
import com.besheater.training.javaxml.entity.hotel.room.Room;
import com.besheater.training.javaxml.entity.hotel.room.RoomFacility;
import com.besheater.training.javaxml.entity.transport.Baggage;
import com.besheater.training.javaxml.entity.transport.LengthUnit;
import com.besheater.training.javaxml.entity.transport.MassUnit;
import com.besheater.training.javaxml.entity.transport.Transport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.net.URL;
import java.util.Currency;
import java.util.Spliterators;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class TouristVouchersParserStAX implements TouristVouchersParser {
    private static final Logger LOG = LogManager.getLogger();

    private XMLEventReader reader;
    boolean isAllXmlProcessed = false;
    boolean isTouristVoucherFullyParsed = false;

    private TouristVoucher touristVoucher;
    private Destination destination;
    private Duration duration;
    private Transport transport;
    private Hotel hotel;
    private Cost cost;

    private Baggage baggage;
    private Meal meal;
    private Service service;
    private Facility facility;
    private Room room;
    private Bed bed;
    private RoomFacility roomFacility;

    @Override
    public Stream<TouristVoucher> parse(URL xml) throws ParseException {
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            reader = factory.createFilteredReader(
                     factory.createXMLEventReader(xml.openStream()),
                     new EventFilterStAX());

        } catch (Exception ex) {
            LOG.error("Parsing failed", ex);
            throw new ParseException("Parsing failed", ex);
        }
        return StreamSupport.stream(new Spliterator(), false);
    }

    private TouristVoucher getNextTouristVoucher() {
        try {
            while (!isTouristVoucherFullyParsed && reader.hasNext()) {
                XMLEvent nextEvent = reader.nextEvent();
                if (nextEvent.isStartElement()) {
                    processStartElement(nextEvent.asStartElement());
                } else if (nextEvent.isEndElement()) {
                    processEndElement(nextEvent.asEndElement());
                } else if (nextEvent.isEndDocument()) {
                    isAllXmlProcessed = true;
                }
            }
        } catch (Exception ex) {
            LOG.error("Can't get next TouristVoucher", ex);
            throw new ParseException("Can't get next TouristVoucher", ex);
        }
        isTouristVoucherFullyParsed = false;
        return touristVoucher;
    }

    private void processStartElement(StartElement element) throws XMLStreamException {
        switch (element.getName().getLocalPart()) {
            case "touristvoucher":
                clearAllVariables();
                long id = Long.parseLong(getAttribute(element, "id"));
                String type = getAttribute(element, "type");
                touristVoucher = new TouristVoucher(id, type);
                break;
            case "destination":
                destination = new Destination();
                break;
            case "country":
                destination.setCountry(getElementContent());
                break;
            case "province":
                destination.setProvince(getElementContent());
                break;
            case "city":
                destination.setCity(getElementContent());
                break;
            case "duration":
                duration = new Duration();
                break;
            case "days":
                duration.setDays(Integer.parseInt(getElementContent()));
                break;
            case "nights":
                duration.setNights(Integer.parseInt(getElementContent()));
                break;
            case "transport":
                transport = new Transport();
                transport.setType(getAttribute(element, "type"));
                break;
            case "class":
                transport.setTravelClass(getElementContent());
                break;
            case "baggage":
                baggage = new Baggage();
                baggage.setType(getAttribute(element, "type"));
                break;
            case "size":
                baggage.setLengthUnit(LengthUnit.parse(getAttribute(element, "unit")));
                break;
            case "total":
                baggage.setTotalDimension(Integer.parseInt(getElementContent()));
                break;
            case "dimensions":
                baggage.setDimensions(getElementContent());
                break;
            case "weight":
                baggage.setMassUnit(MassUnit.parse(getAttribute(element, "unit")));
                baggage.setWeight(Integer.parseInt(getElementContent()));
                break;
            case "hotel":
                if (cost == null) {
                    // <hotel> inside <touristvoucher>
                    hotel = new Hotel();
                    hotel.setId(getAttribute(element, "id"));
                    hotel.setStarRating(Integer.parseInt(getAttribute(element, "starrating")));
                } else {
                    // <hotel> inside <cost>
                    cost.setHotelCost(Double.parseDouble(getElementContent()));
                }
                break;
            case "meal":
                meal = new Meal();
                break;
            case "boardbasis":
                meal.setBoardBasis(getElementContent());
                break;
            case "specialrequest":
                meal.setSpecialRequest(getElementContent());
                break;
            case "services":
                break;
            case "service":
                service = new Service();
                service.setType(getAttribute(element, "type"));
                service.setDescription(getElementContent());
                break;
            case "facilities":
                break;
            case "facility":
                if (room == null) {
                    // <facility> inside <hotel>
                    facility = new Facility();
                    facility.setType(getAttribute(element, "type"));
                    facility.setDescription(getElementContent());
                } else {
                    // <facility> inside <room>
                    roomFacility = new RoomFacility();
                    String quantity = getAttribute(element, "quantity");
                    if (quantity != null) {
                        roomFacility.setQuantity(Integer.parseInt(quantity));
                    }
                    roomFacility.setDescription(getElementContent());
                }
                break;
            case "room":
                room = new Room();
                break;
            case "sleeps":
                room.setSleeps(Integer.parseInt(getAttribute(element, "quantity")));
                break;
            case "beds":
                break;
            case "bed":
                bed = new Bed();
                bed.setType(BedType.parse(getAttribute(element, "type")));
                bed.setQuantity(Integer.parseInt(getAttribute(element, "quantity")));
                break;
            case "cost":
                cost = new Cost();
                cost.setCurrency(Currency.getInstance(getAttribute(element, "currency")));
                break;
            case "travel":
                cost.setTravelCost(Double.parseDouble(getElementContent()));
                break;
            case "activities":
                cost.setActivitiesCost(Double.parseDouble(getElementContent()));
                break;
        }

    }

    private void processEndElement(EndElement element) {
        switch (element.getName().getLocalPart()) {
            case "touristvoucher":
                isTouristVoucherFullyParsed = true;
                break;
            case "destination":
                touristVoucher.setDestination(destination);
                break;
            case "country":
                break;
            case "province":
                break;
            case "city":
                break;
            case "duration":
                touristVoucher.setDuration(duration);
                break;
            case "days":
                break;
            case "nights":
                break;
            case "transport":
                touristVoucher.setTransport(transport);
                break;
            case "class":
                break;
            case "baggage":
                transport.addBaggage(baggage);
                break;
            case "size":
                break;
            case "total":
                break;
            case "dimensions":
                break;
            case "weight":
                break;
            case "hotel":
                if (cost == null) {
                    // <hotel> inside <touristvoucher>
                    touristVoucher.setHotel(hotel);
                } else {
                    // <hotel> inside <cost>
                }
                break;
            case "meal":
                hotel.setMeal(meal);
                break;
            case "boardbasis":
                break;
            case "specialrequest":
                break;
            case "services":
                break;
            case "service":
                hotel.addService(service);
                break;
            case "facilities":
                break;
            case "facility":
                if (room == null) {
                    // <facility> inside <hotel>
                    hotel.addFacility(facility);
                } else {
                    // <facility> inside <room>
                    room.addFacility(roomFacility);
                }
                break;
            case "room":
                hotel.setRoom(room);
                break;
            case "sleeps":
                break;
            case "beds":
                break;
            case "bed":
                room.addBed(bed);
                break;
            case "cost":
                touristVoucher.setCost(cost);
                break;
            case "travel":
                break;
            case "activities":
                break;
        }
    }

    private String getAttribute(StartElement element, String name) {
        Attribute attribute = element.getAttributeByName(QName.valueOf(name));
        if (attribute != null) {
            return attribute.getValue();
        }
        return null;
    }

    private String getElementContent() throws XMLStreamException {
        return reader.nextEvent().asCharacters().getData();
    }

    private void clearAllVariables() {
        touristVoucher = null;
        destination = null;
        duration = null;
        transport = null;
        hotel = null;
        cost = null;

        baggage = null;
        meal = null;
        service = null;
        facility = null;
        room = null;
        bed = null;
        roomFacility = null;
    }

    private class Spliterator extends Spliterators.AbstractSpliterator<TouristVoucher> {

        protected Spliterator() {
            super(Long.MAX_VALUE, Spliterator.ORDERED);
        }

        @Override
        public boolean tryAdvance(Consumer<? super TouristVoucher> action) {
            TouristVoucher touristVoucher = getNextTouristVoucher();
            if (isAllXmlProcessed) {
                return false;
            }
            action.accept(touristVoucher);
            return true;
        }
    }
}