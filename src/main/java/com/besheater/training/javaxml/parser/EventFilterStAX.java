package com.besheater.training.javaxml.parser;

import javax.xml.stream.EventFilter;
import javax.xml.stream.events.XMLEvent;

public class EventFilterStAX implements EventFilter {

    @Override
    public boolean accept(XMLEvent event) {
        return event.isStartElement() || event.isEndElement() ||
                event.isCharacters() || event.isEndDocument();
    }
}
