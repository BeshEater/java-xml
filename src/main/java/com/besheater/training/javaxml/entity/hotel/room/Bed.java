package com.besheater.training.javaxml.entity.hotel.room;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Bed {
    private static final Logger LOG = LogManager.getLogger();

    private BedType type;
    private int quantity;

    public Bed() {
        LOG.debug("New instance created");
    }

    public Bed(BedType type, int quantity) {
        this.type = type;
        this.quantity = quantity;
        LOG.debug("New instance created");
    }

    public BedType getType() {
        return type;
    }

    public void setType(BedType type) {
        this.type = type;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Bed{" +
                "type='" + type + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}