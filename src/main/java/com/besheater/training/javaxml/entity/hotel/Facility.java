package com.besheater.training.javaxml.entity.hotel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Facility {
    private static final Logger LOG = LogManager.getLogger();

    private String type;
    private String description;

    public Facility() {
        LOG.debug("New instance created");
    }

    public Facility(String type, String description) {
        this.type = type;
        this.description = description;
        LOG.debug("New instance created");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Facility{" +
                "type='" + type + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}