package com.besheater.training.javaxml.entity.hotel.room;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class RoomFacility {
    private static final Logger LOG = LogManager.getLogger();

    private int quantity = 1;
    private String description;

    public RoomFacility() {
        LOG.debug("New instance created");
    }

    public RoomFacility(String description) {
        this.description = description;
        LOG.debug("New instance created");
    }

    public RoomFacility(int quantity, String description) {
        this.quantity = quantity;
        this.description = description;
        LOG.debug("New instance created");
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Facility{" +
                "quantity=" + quantity +
                ", description='" + description + '\'' +
                '}';
    }
}