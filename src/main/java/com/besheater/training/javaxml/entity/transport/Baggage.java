package com.besheater.training.javaxml.entity.transport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Baggage {
    private static final Logger LOG = LogManager.getLogger();

    private String type;
    private MassUnit massUnit;
    private int weight;
    private LengthUnit lengthUnit;
    private String dimensions;
    private Integer totalDimension;

    public Baggage() {
        LOG.debug("New instance created");
    }

    public Baggage(String type, MassUnit massUnit, int weight,
                   LengthUnit lengthUnit, String dimensions) {
        this.type = type;
        this.massUnit = massUnit;
        this.weight = weight;
        this.lengthUnit = lengthUnit;
        this.dimensions = dimensions;
        this.totalDimension = null;
        LOG.debug("New instance created");
    }

    public Baggage(String type, MassUnit massUnit, int weight,
                   LengthUnit lengthUnit, int totalDimension) {
        this.type = type;
        this.massUnit = massUnit;
        this.weight = weight;
        this.lengthUnit = lengthUnit;
        this.dimensions = null;
        this.totalDimension = totalDimension;
        LOG.debug("New instance created");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public MassUnit getMassUnit() {
        return massUnit;
    }

    public void setMassUnit(MassUnit massUnit) {
        this.massUnit = massUnit;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public LengthUnit getLengthUnit() {
        return lengthUnit;
    }

    public void setLengthUnit(LengthUnit lengthUnit) {
        this.lengthUnit = lengthUnit;
    }

    public String getDimensions() {
        return dimensions;
    }

    public void setDimensions(String dimensions) {
        this.dimensions = dimensions;
    }

    public Integer getTotalDimension() {
        return totalDimension;
    }

    public void setTotalDimension(Integer totalDimension) {
        this.totalDimension = totalDimension;
    }

    @Override
    public String toString() {
        return "Baggage{" +
                "type='" + type + '\'' +
                ", massUnit=" + massUnit +
                ", weight=" + weight +
                ", lengthUnit=" + lengthUnit +
                ", dimensions='" + dimensions + '\'' +
                ", totalDimension=" + totalDimension +
                '}';
    }
}