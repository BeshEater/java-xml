package com.besheater.training.javaxml.entity;

import com.besheater.training.javaxml.entity.hotel.Hotel;
import com.besheater.training.javaxml.entity.transport.Transport;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class TouristVoucher {
    private static final Logger LOG = LogManager.getLogger();

    private final long id;
    private final String type;
    private Destination destination;
    private Duration duration;
    private Transport transport;
    private Hotel hotel;
    private Cost cost;

    public TouristVoucher(long id, String type) {
        this.id = id;
        this.type = type;
        LOG.debug("New instance created");
    }

    public long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Duration getDuration() {
        return duration;
    }

    public void setDuration(Duration duration) {
        this.duration = duration;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Cost getCost() {
        return cost;
    }

    public void setCost(Cost cost) {
        this.cost = cost;
    }

    @Override
    public String toString() {
        return "TouristVoucher{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", destination=" + destination +
                ", duration=" + duration +
                ", transport=" + transport +
                ", hotel=" + hotel +
                ", cost=" + cost +
                '}';
    }
}