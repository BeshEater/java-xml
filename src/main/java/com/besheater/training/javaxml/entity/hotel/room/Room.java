package com.besheater.training.javaxml.entity.hotel.room;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Room {
    private static final Logger LOG = LogManager.getLogger();

    private int sleeps;
    private List<Bed> beds = new ArrayList<>();
    private List<RoomFacility> facilities = new ArrayList<>();

    public Room() {
        LOG.debug("New instance created");
    }

    public Room(int sleeps, List<Bed> beds, List<RoomFacility> facilities) {
        this.sleeps = sleeps;
        this.beds = beds;
        this.facilities = facilities;
        LOG.debug("New instance created");
    }

    public int getSleeps() {
        return sleeps;
    }

    public void setSleeps(int sleeps) {
        this.sleeps = sleeps;
    }

    public List<Bed> getBeds() {
        return beds;
    }

    public void setBeds(List<Bed> beds) {
        this.beds = beds;
    }

    public boolean addBed(Bed bed) {
        return beds.add(bed);
    }

    public List<RoomFacility> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<RoomFacility> facilities) {
        this.facilities = facilities;
    }

    public boolean addFacility(RoomFacility facility) {
        return facilities.add(facility);
    }

    @Override
    public String toString() {
        return "Room{" +
                "sleeps=" + sleeps +
                ", beds=" + beds +
                ", facilities=" + facilities +
                '}';
    }
}