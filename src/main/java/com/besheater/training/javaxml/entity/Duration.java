package com.besheater.training.javaxml.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Duration {
    private static final Logger LOG = LogManager.getLogger();

    private int days;
    private int nights;

    public Duration() {
        LOG.debug("New instance created");
    }

    public Duration(int days, int nights) {
        this.days = days;
        this.nights = nights;
        LOG.debug("New instance created");
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }

    public int getNights() {
        return nights;
    }

    public void setNights(int nights) {
        this.nights = nights;
    }

    @Override
    public String toString() {
        return "Duration{" +
                "days=" + days +
                ", nights=" + nights +
                '}';
    }
}