package com.besheater.training.javaxml.entity.hotel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Meal {
    private static final Logger LOG = LogManager.getLogger();

    private String boardBasis;
    private String specialRequest;

    public Meal() {
        LOG.debug("New instance created");
    }

    public Meal(String boardBasis, String specialRequest) {
        this.boardBasis = boardBasis;
        this.specialRequest = specialRequest;
        LOG.debug("New instance created");
    }

    public String getBoardBasis() {
        return boardBasis;
    }

    public void setBoardBasis(String boardBasis) {
        this.boardBasis = boardBasis;
    }

    public String getSpecialRequest() {
        return specialRequest;
    }

    public void setSpecialRequest(String specialRequest) {
        this.specialRequest = specialRequest;
    }

    @Override
    public String toString() {
        return "Meal{" +
                "boardBasis='" + boardBasis + '\'' +
                ", specialRequest='" + specialRequest + '\'' +
                '}';
    }
}