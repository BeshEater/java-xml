package com.besheater.training.javaxml.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Currency;

public class Cost {
    private static final Logger LOG = LogManager.getLogger();

    private Currency currency;
    private double travelCost;
    private double hotelCost;
    private double activitiesCost;

    public Cost() {
        LOG.debug("New instance created");
    }

    public Cost(Currency currency, double travelCost, double hotelCost, double activitiesCost) {
        this.currency = currency;
        this.travelCost = travelCost;
        this.hotelCost = hotelCost;
        this.activitiesCost = activitiesCost;
        LOG.debug("New instance created");
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public double getTravelCost() {
        return travelCost;
    }

    public void setTravelCost(double travelCost) {
        this.travelCost = travelCost;
    }

    public double getHotelCost() {
        return hotelCost;
    }

    public void setHotelCost(double hotelCost) {
        this.hotelCost = hotelCost;
    }

    public double getActivitiesCost() {
        return activitiesCost;
    }

    public void setActivitiesCost(double activitiesCost) {
        this.activitiesCost = activitiesCost;
    }

    public double getTotalCost() {
        return travelCost + hotelCost + activitiesCost;
    }

    @Override
    public String toString() {
        return "Cost{" +
                "currency=" + currency +
                ", travelCost=" + travelCost +
                ", hotelCost=" + hotelCost +
                ", activitiesCost=" + activitiesCost +
                '}';
    }
}