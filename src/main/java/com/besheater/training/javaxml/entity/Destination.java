package com.besheater.training.javaxml.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Destination {
    private static final Logger LOG = LogManager.getLogger();

    private String country;
    private String province;
    private String city;

    public Destination() {
        LOG.debug("New instance created");
    }

    public Destination(String country, String province, String city) {
        this.country = country;
        this.province = province;
        this.city = city;
        LOG.debug("New instance created");
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Destination{" +
                "country='" + country + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                '}';
    }
}