package com.besheater.training.javaxml.entity.transport;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Transport {
    private static final Logger LOG = LogManager.getLogger();

    private String type;
    private String travelClass;
    private List<Baggage> baggage = new ArrayList<>();

    public Transport() {
        LOG.debug("New instance created");
    }

    public Transport(String type, String travelClass, List<Baggage> baggage) {
        this.type = type;
        this.travelClass = travelClass;
        this.baggage = baggage;
        LOG.debug("New instance created");
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTravelClass() {
        return travelClass;
    }

    public void setTravelClass(String travelClass) {
        this.travelClass = travelClass;
    }

    public List<Baggage> getBaggage() {
        return baggage;
    }

    public void setBaggage(List<Baggage> baggage) {
        this.baggage = baggage;
    }

    public boolean addBaggage(Baggage baggage) {
        return this.baggage.add(baggage);
    }

    @Override
    public String toString() {
        return "Transport{" +
                "type='" + type + '\'' +
                ", travelClass='" + travelClass + '\'' +
                ", baggage=" + baggage +
                '}';
    }
}