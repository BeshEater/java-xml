package com.besheater.training.javaxml.entity.transport;

import java.util.HashMap;
import java.util.Map;

public enum LengthUnit {
    CENTIMETER("cm"),
    INCH("in");

    private static Map<String, LengthUnit> tagNames = new HashMap<>();
    static {
        for(LengthUnit lengthUnit : LengthUnit.values()) {
            tagNames.put(lengthUnit.tagName, lengthUnit);
        }
    }
    private String tagName;

    LengthUnit(String tagName) {
        this.tagName = tagName;
    }

    public String getTagName() {
        return tagName;
    }

    public static LengthUnit parse(String tagName) {
        return tagNames.get(tagName);
    }
}