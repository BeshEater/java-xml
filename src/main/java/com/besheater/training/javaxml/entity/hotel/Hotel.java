package com.besheater.training.javaxml.entity.hotel;

import com.besheater.training.javaxml.entity.hotel.room.Room;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Hotel {
    private static final Logger LOG = LogManager.getLogger();

    private String id;
    private int starRating;
    private Meal meal;
    private List<Service> services = new ArrayList<>();
    private List<Facility> facilities = new ArrayList<>();
    private Room room;

    public Hotel() {
        LOG.debug("New instance created");
    }

    public Hotel(String id, int starRating, Meal meal,
                 List<Service> services, List<Facility> facilities, Room room) {
        this.id = id;
        this.starRating = starRating;
        this.meal = meal;
        this.services = services;
        this.facilities = facilities;
        this.room = room;
        LOG.debug("New instance created");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getStarRating() {
        return starRating;
    }

    public void setStarRating(int starRating) {
        this.starRating = starRating;
    }

    public Meal getMeal() {
        return meal;
    }

    public void setMeal(Meal meal) {
        this.meal = meal;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

    public boolean addService(Service service) {
        return services.add(service);
    }

    public List<Facility> getFacilities() {
        return facilities;
    }

    public void setFacilities(List<Facility> facilities) {
        this.facilities = facilities;
    }

    public boolean addFacility(Facility facility) {
        return facilities.add(facility);
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "id='" + id + '\'' +
                ", starRating=" + starRating +
                ", meal=" + meal +
                ", services=" + services +
                ", facilities=" + facilities +
                ", room=" + room +
                '}';
    }
}