package com.besheater.training.javaxml.entity.hotel.room;

import java.util.HashMap;
import java.util.Map;

public enum BedType {
    SINGLE("single", 1),
    TWIN("twin", 1),
    DOUBLE("double", 1),
    FULL("full", 2),
    QUEEN("queen", 2),
    KING("king", 2);

    private static Map<String, BedType> tagNames = new HashMap<>();
    static {
        for(BedType bedType : BedType.values()) {
            tagNames.put(bedType.tagName, bedType);
        }
    }
    private String tagName;
    private final int sleeps;

    BedType(String tagName, int sleeps) {
        this.tagName = tagName;
        this.sleeps = sleeps;
    }

    public int getSleeps() {
        return sleeps;
    }

    public String getTagName() {
        return tagName;
    }

    public static BedType parse(String tagName) {
        return tagNames.get(tagName);
    }
}
