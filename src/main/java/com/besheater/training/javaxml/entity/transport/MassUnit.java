package com.besheater.training.javaxml.entity.transport;

import java.util.HashMap;
import java.util.Map;

public enum MassUnit {
    KILOGRAM("kg"),
    POUND("lb");

    private static Map<String, MassUnit> tagNames = new HashMap<>();
    static {
        for(MassUnit massUnit : MassUnit.values()) {
            tagNames.put(massUnit.tagName, massUnit);
        }
    }
    private String tagName;

    MassUnit(String tagName) {
        this.tagName = tagName;
    }

    public String getTagName() {
        return tagName;
    }

    public static MassUnit parse(String tagName) {
        return tagNames.get(tagName);
    }
}