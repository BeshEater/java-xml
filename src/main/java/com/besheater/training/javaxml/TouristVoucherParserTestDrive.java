package com.besheater.training.javaxml;

import com.besheater.training.javaxml.entity.Cost;
import com.besheater.training.javaxml.entity.Destination;
import com.besheater.training.javaxml.entity.TouristVoucher;
import com.besheater.training.javaxml.generator.TouristVoucherGenerator;
import com.besheater.training.javaxml.generator.TouristVouchersXMLGenerator;
import com.besheater.training.javaxml.parser.TouristVouchersUniversalParser;
import com.besheater.training.javaxml.parser.XMLparserType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.transform.TransformerException;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingDouble;

public class TouristVoucherParserTestDrive {
    private static final Logger LOG = LogManager.getLogger();

    public static void main(String[] args) {
        URL xml = TouristVoucherParserTestDrive.class.getResource("/touristvouchers.xml");
        URL xsd = TouristVoucherParserTestDrive.class.getResource("/touristvouchers.xsd");

        // Create DOM parser
        TouristVouchersUniversalParser parserDOM = new TouristVouchersUniversalParser.Builder()
                                    .setParserType(XMLparserType.DOM)
                                    .setXML(xml)
                                    .setXSD(xsd)
                                    .build();

        // Create SAX parser
        TouristVouchersUniversalParser parserSAX = new TouristVouchersUniversalParser.Builder()
                                    .setParserType(XMLparserType.SAX)
                                    .setXML(xml)
                                    .setXSD(xsd)
                                    .build();

        // Create StAX parser
        TouristVouchersUniversalParser parserStAX = new TouristVouchersUniversalParser.Builder()
                                    .setParserType(XMLparserType.STAX)
                                    .setXML(xml)
                                    .setXSD(xsd)
                                    .build();

        // Just extract some random data from xml using created parsers

        // Find tourist vouchers count
        long vouchersCount = parserDOM.parse().count();
        System.out.println("Total tourist vouchers count:");
        System.out.println(vouchersCount);
        System.out.println("");

        // Find the most expensive tourist voucher cost
        Cost maxCost = parserSAX.parse()
                                 .max(comparingDouble(v -> v.getCost().getTotalCost()))
                                 .map(TouristVoucher::getCost)
                                 .get();
        System.out.println("The most expensive tourist voucher cost:");
        System.out.printf("%.2f %s\n", maxCost.getTotalCost(), maxCost.getCurrency());
        System.out.println("");

        // Find all available countries
        List<String> availableCountries = parserStAX.parse().map(TouristVoucher::getDestination)
                                                           .map(Destination::getCountry)
                                                           .distinct()
                                                           .collect(Collectors.toList());
        System.out.println("Available countries:");
        availableCountries.forEach(System.out::println);
        System.out.println("");

        // Find all tourist vouchers with 5 star hotel rating
        List<TouristVoucher> topHotelVouchers = parserStAX.parse()
                                                .filter(v -> v.getHotel().getStarRating() == 5)
                                                .collect(Collectors.toList());
        System.out.println("Tourist vouchers with 5 star hotel rating:");
        topHotelVouchers.forEach(System.out::println);

    }

    public static void makeTouristVouchersXMLFile(int vouchersCount) {
        List<TouristVoucher> touristVouchers = new ArrayList<>();
        TouristVoucherGenerator generator = new TouristVoucherGenerator();
        for (int i = 0; i < vouchersCount; i++) {
            touristVouchers.add(generator.next());
        }
        File file = new File("touristvouchers.xml");
        TouristVouchersXMLGenerator xmlGenerator = new TouristVouchersXMLGenerator();
        try {
            xmlGenerator.write(touristVouchers, file);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}