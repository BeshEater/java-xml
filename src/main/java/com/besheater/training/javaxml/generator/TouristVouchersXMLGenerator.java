package com.besheater.training.javaxml.generator;

import com.besheater.training.javaxml.entity.Cost;
import com.besheater.training.javaxml.entity.Destination;
import com.besheater.training.javaxml.entity.Duration;
import com.besheater.training.javaxml.entity.TouristVoucher;
import com.besheater.training.javaxml.entity.hotel.Facility;
import com.besheater.training.javaxml.entity.hotel.Hotel;
import com.besheater.training.javaxml.entity.hotel.Meal;
import com.besheater.training.javaxml.entity.hotel.Service;
import com.besheater.training.javaxml.entity.hotel.room.Bed;
import com.besheater.training.javaxml.entity.hotel.room.Room;
import com.besheater.training.javaxml.entity.hotel.room.RoomFacility;
import com.besheater.training.javaxml.entity.transport.Baggage;
import com.besheater.training.javaxml.entity.transport.Transport;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.util.List;

import static java.lang.String.valueOf;

public class TouristVouchersXMLGenerator {
    private Document document;

    public TouristVouchersXMLGenerator() {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            document = documentBuilder.newDocument();
        } catch (ParserConfigurationException ex) {
            throw new RuntimeException(ex);
        }

    }

    public void write(List<TouristVoucher> touristVouchers, File file) throws TransformerException {
        Element touristVouchersElem = makeTouristVouchersElement();
        document.appendChild(touristVouchersElem);

        for (TouristVoucher touristVoucher: touristVouchers) {
            addTouristVoucher(touristVouchersElem, touristVoucher);
        }

        writeToFile(file);
    }

    private void writeToFile(File file) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.transform(new DOMSource(document), new StreamResult(file));
    }

    private void addTouristVoucher(Element touristvouchersElem, TouristVoucher voucher) {
        Element touristVoucherElem = makeTouristVoucherElement(voucher);
        touristvouchersElem.appendChild(touristVoucherElem);

        Element destinationElem = makeDestinationElement(voucher.getDestination());
        touristVoucherElem.appendChild(destinationElem);

        Element durationElem = makeDurationElement(voucher.getDuration());
        touristVoucherElem.appendChild(durationElem);

        Element transportElem = makeTransportElement(voucher.getTransport());
        touristVoucherElem.appendChild(transportElem);

        Element hotelElem = makeHotelElement(voucher.getHotel());
        touristVoucherElem.appendChild(hotelElem);

        Element costElem = makeCostElement(voucher.getCost());
        touristVoucherElem.appendChild(costElem);
    }

    private Element makeTouristVouchersElement() {
        Element touristvouchersElem = createElement("touristvouchers");
        touristvouchersElem.setAttribute("xmlns",
                "http://www.besheater.com/training/java/xml");
        touristvouchersElem.setAttribute("xmlns:xsi",
                "http://www.w3.org/2001/XMLSchema-instance");
        touristvouchersElem.setAttribute("xsi:schemaLocation",
                "http://www.besheater.com/training/java/xml touristvouchers.xsd");
        return touristvouchersElem;
    }

    private Element makeTouristVoucherElement(TouristVoucher voucher) {
        Element touristVoucherElem = createElement("touristvoucher");
        touristVoucherElem.setAttribute("id", String.valueOf(voucher.getId()));
        touristVoucherElem.setAttribute("type", voucher.getType());
        return touristVoucherElem;
    }

    private Element makeDestinationElement(Destination destination) {
        Element destinationElem = createElement("destination");
        Element countryElem = createElement("country");
        countryElem.appendChild(createTextNode(destination.getCountry()));
        Element provinceElem = createElement("province");
        provinceElem.appendChild(createTextNode(destination.getProvince()));
        Element cityElem = createElement("city");
        cityElem.appendChild(createTextNode(destination.getCity()));
        destinationElem.appendChild(countryElem);
        destinationElem.appendChild(provinceElem);
        destinationElem.appendChild(cityElem);
        return destinationElem;
    }

    private Element makeDurationElement(Duration duration) {
        Element durationElem = createElement("duration");
        Element daysElem = createElement("days");
        daysElem.appendChild(createTextNode(valueOf(duration.getDays())));
        Element nightsElem = createElement("nights");
        nightsElem.appendChild(createTextNode(valueOf(duration.getNights())));
        durationElem.appendChild(daysElem);
        durationElem.appendChild(nightsElem);
        return durationElem;
    }

    private Element makeTransportElement(Transport transport) {
        Element transportElem = createElement("transport");
        transportElem.setAttribute("type", transport.getType());
        Element classElem = createElement("class");
        classElem.appendChild(createTextNode(transport.getTravelClass()));
        transportElem.appendChild(classElem);
        for (Baggage baggage : transport.getBaggage()) {
            Element baggageElem = makeBaggageElement(baggage);
            transportElem.appendChild(baggageElem);
        }
        return transportElem;
    }

    private Element makeBaggageElement(Baggage baggage) {
        Element baggageElem = createElement("baggage");
        baggageElem.setAttribute("type", baggage.getType());
        Element sizeElem = createElement("size");
        sizeElem.setAttribute("unit", baggage.getLengthUnit().getTagName());
        if (baggage.getTotalDimension() != null) {
            Element totalElem = createElement("total");
            totalElem.appendChild(createTextNode(valueOf(baggage.getTotalDimension())));
            sizeElem.appendChild(totalElem);
        }
        if (baggage.getDimensions() != null) {
            Element dimensionsElem = createElement("dimensions");
            dimensionsElem.appendChild(createTextNode(baggage.getDimensions()));
            sizeElem.appendChild(dimensionsElem);
        }
        Element weightElem = createElement("weight");
        weightElem.setAttribute("unit", baggage.getMassUnit().getTagName());
        weightElem.appendChild(createTextNode(valueOf(baggage.getWeight())));
        baggageElem.appendChild(sizeElem);
        baggageElem.appendChild(weightElem);
        return baggageElem;
    }

    private Element makeHotelElement(Hotel hotel) {
        Element hotelElem = createElement("hotel");
        hotelElem.setAttribute("id", hotel.getId());
        hotelElem.setAttribute("starrating", valueOf(hotel.getStarRating()));

        Element mealElem = makeMealElement(hotel.getMeal());
        Element servicesElem = makeServicesElement(hotel.getServices());
        Element facilitiesElem = makeFacilitiesElement(hotel.getFacilities());
        Element roomElem = makeRoomElement(hotel.getRoom());

        hotelElem.appendChild(mealElem);
        hotelElem.appendChild(servicesElem);
        hotelElem.appendChild(facilitiesElem);
        hotelElem.appendChild(roomElem);
        return hotelElem;
    }

    private Element makeMealElement(Meal meal) {
        Element mealElem = createElement("meal");
        Element boardBasisElem = createElement("boardbasis");
        boardBasisElem.appendChild(createTextNode(meal.getBoardBasis()));
        mealElem.appendChild(boardBasisElem);
        if (meal.getSpecialRequest() != null) {
            Element specialRequestElem = createElement("specialrequest");
            specialRequestElem.appendChild(createTextNode(meal.getSpecialRequest()));
            mealElem.appendChild(specialRequestElem);
        }
        return mealElem;
    }

    private Element makeServicesElement(List<Service> services) {
        Element servicesElem = createElement("services");
        for (Service service : services) {
            Element serviceElem = createElement("service");
            serviceElem.setAttribute("type", service.getType());
            serviceElem.appendChild(createTextNode(service.getDescription()));
            servicesElem.appendChild(serviceElem);
        }
        return servicesElem;
    }

    private Element makeFacilitiesElement(List<Facility> facilities) {
        Element facilitiesElem = createElement("facilities");
        for (Facility facility : facilities) {
            Element facilityElem = createElement("facility");
            facilityElem.setAttribute("type", facility.getType());
            facilityElem.appendChild(createTextNode(facility.getDescription()));
            facilitiesElem.appendChild(facilityElem);
        }
        return facilitiesElem;
    }

    private Element makeRoomElement(Room room) {
        Element roomElem = createElement("room");
        Element sleepsElem = createElement("sleeps");
        sleepsElem.setAttribute("quantity", valueOf(room.getSleeps()));
        Element bedsElem = makeBedsElement(room.getBeds());
        Element facilitiesElem = makeRoomFacilities(room.getFacilities());

        roomElem.appendChild(sleepsElem);
        roomElem.appendChild(bedsElem);
        roomElem.appendChild(facilitiesElem);
        return roomElem;
    }

    private Element makeBedsElement(List<Bed> beds) {
        Element bedsElem = createElement("beds");
        for (Bed bed : beds) {
            Element bedElem = createElement("bed");
            bedElem.setAttribute("type", bed.getType().getTagName());
            bedElem.setAttribute("quantity", valueOf(bed.getQuantity()));
            bedsElem.appendChild(bedElem);
        }
        return bedsElem;
    }

    private Element makeRoomFacilities(List<RoomFacility> facilities) {
        Element facilitiesElem = createElement("facilities");
        for (RoomFacility facility : facilities) {
            Element facilityElem = createElement("facility");
            if (facility.getQuantity() > 1) {
                facilityElem.setAttribute("quantity", valueOf(facility.getQuantity()));
            }
            facilityElem.appendChild(createTextNode(facility.getDescription()));
            facilitiesElem.appendChild(facilityElem);
        }
        return facilitiesElem;
    }

    private Element makeCostElement(Cost cost) {
        Element costElem = createElement("cost");
        costElem.setAttribute("currency", cost.getCurrency().getCurrencyCode());
        Element travelElem = createElement("travel");
        travelElem.appendChild(createTextNode(valueOf(cost.getTravelCost())));
        Element hotelElem = createElement("hotel");
        hotelElem.appendChild(createTextNode(valueOf(cost.getHotelCost())));
        Element activitiesElem = createElement("activities");
        activitiesElem.appendChild(createTextNode(valueOf(cost.getActivitiesCost())));
        costElem.appendChild(travelElem);
        costElem.appendChild(hotelElem);
        costElem.appendChild(activitiesElem);
        return costElem;
    }

    private Element createElement(String name) {
        return document.createElement(name);
    }

    private Text createTextNode(String data) {
        return document.createTextNode(data);
    }
}