package com.besheater.training.javaxml.generator;

import com.besheater.training.javaxml.entity.Cost;
import com.besheater.training.javaxml.entity.Destination;
import com.besheater.training.javaxml.entity.Duration;
import com.besheater.training.javaxml.entity.TouristVoucher;
import com.besheater.training.javaxml.entity.hotel.Facility;
import com.besheater.training.javaxml.entity.hotel.Hotel;
import com.besheater.training.javaxml.entity.hotel.Meal;
import com.besheater.training.javaxml.entity.hotel.Service;
import com.besheater.training.javaxml.entity.hotel.room.Bed;
import com.besheater.training.javaxml.entity.hotel.room.BedType;
import com.besheater.training.javaxml.entity.hotel.room.Room;
import com.besheater.training.javaxml.entity.hotel.room.RoomFacility;
import com.besheater.training.javaxml.entity.transport.Baggage;
import com.besheater.training.javaxml.entity.transport.LengthUnit;
import com.besheater.training.javaxml.entity.transport.MassUnit;
import com.besheater.training.javaxml.entity.transport.Transport;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicLong;

public class TouristVoucherGenerator {
    private ThreadLocalRandom random = ThreadLocalRandom.current();
    private final double checkedBaggageChance = 0.7;
    private final double dimensionsBaggageChance = 0.5;
    private final double specialRequestChance = 0.2;

    private final AtomicLong touristVoucherIdGenerator = new AtomicLong(1000);
    private final AtomicLong hotelIdGenerator = new AtomicLong(42);
    private String[] touristVoucherTypes = {"weekend", "excursion", "recreation", "pilgrimage"};
    private Destination[] destinations = {
            new Destination("IT", "Lombardy", "Milan"),
            new Destination("GB", "Manchester", "Manchester"),
            new Destination("FR", "Ile-de-France", "Paris"),
            new Destination("GE", "Darmstadt", "Frankfurt"),
            new Destination("CA", "Ontario", "Toronto"),
            new Destination("US", "New York", "New York"),
            new Destination("EG", "Cairo", "Cairo") };
    private String[] transportTypes = {"aviation", "rail-road", "car", "bus", "ship"};
    private String[] travelClasses = {"A1", "A2", "B1", "B2", "C"};
    private String[] boardBasises = {"RO", "SC", "BB", "HB", "FB", "AL"};
    private String[] specialRequests = {"Halal", "Kosher", "Vegetarian", "Allergy"};
    private Service[] services = {
            new Service("cleaning", "Daily housekeeping"),
            new Service("cleaning", "Ironing service"),
            new Service("cleaning", "Dry cleaning"),
            new Service("cleaning", "Laundry"),
            new Service("front-desk", "Concierge"),
            new Service("front-desk", "Tour desk"),
            new Service("front-desk", "Ticket service"),
            new Service("front-desk", "Baggage storage"),
            new Service("security", "Security alarm"),
            new Service("security", "Security alarm"),
            new Service("security", "Security alarm") };
    private Facility[] facilities = {
            new Facility("parking", "Parking garage"),
            new Facility("parking", "Accessible parking"),
            new Facility("pool-and-spa", "Swimming pool"),
            new Facility("pool-and-spa", "Solarium"),
            new Facility("pool-and-spa", "Sauna"),
            new Facility("pool-and-spa", "Beach umbrellas"),
            new Facility("pool-and-spa", "Shallow end"),
            new Facility("activity", "Bicycle rental"),
            new Facility("activity", "Live sports events bar"),
            new Facility("outdoors", "Sun deck"),
            new Facility("outdoors", "Terrace"),
            new Facility("outdoors", "Garden"), };
    private String[] roomFacilitiesDescription = {"TV", "Conditioner", "Microwave", "Telephone",
                                                  "Toilet", "Shower", "Bathtub"};
    private Currency[] currencies = {
            Currency.getInstance("USD"),
            Currency.getInstance("EUR"),
            Currency.getInstance("GBP"),
            Currency.getInstance("CAD"), };

    public TouristVoucher next() {
        long id = touristVoucherIdGenerator.getAndIncrement();
        String type = touristVoucherTypes[random.nextInt(touristVoucherTypes.length)];

        TouristVoucher touristVoucher = new TouristVoucher(id, type);
        touristVoucher.setDestination(getDestination());
        touristVoucher.setDuration(getDuration());
        touristVoucher.setTransport(getTransport());
        touristVoucher.setHotel(getHotel());
        touristVoucher.setCost(getCost());

        return touristVoucher;
    }

    private Destination getDestination() {
        return destinations[random.nextInt(destinations.length)];
    }

    private Duration getDuration() {
        int days = random.nextInt(1, 15);
        int nights = days - 1;
        return new Duration(days, nights);
    }

    private Transport getTransport() {
        Transport transport = new Transport();
        transport.setType(transportTypes[random.nextInt(transportTypes.length)]);
        transport.setTravelClass(travelClasses[random.nextInt(travelClasses.length)]);
        transport.addBaggage(getBaggage("carry"));
        if (random.nextDouble() < checkedBaggageChance) {
            transport.addBaggage(getBaggage("checked"));
        }
        return transport;
    }

    private Baggage getBaggage(String type) {
        Baggage baggage = new Baggage();
        baggage.setType(type);
        LengthUnit lengthUnit = LengthUnit.values()[random.nextInt(LengthUnit.values().length)];
        MassUnit massUnit = MassUnit.values()[random.nextInt(MassUnit.values().length)];
        baggage.setLengthUnit(lengthUnit);
        baggage.setMassUnit(massUnit);
        if (random.nextDouble() < dimensionsBaggageChance) {
            int width = 0;
            int length = 0;
            int height = 0;
            if (lengthUnit == LengthUnit.CENTIMETER) {
                width = random.nextInt(10, 21);
                length = random.nextInt(20, 31);
                height = random.nextInt(35, 61);
            }
            if (lengthUnit == LengthUnit.INCH) {
                width = random.nextInt(5, 11);
                length = random.nextInt(10, 16);
                height = random.nextInt(17, 31);
            }
            baggage.setDimensions(String.format("%d x %d x %d", width, length, height));
        } else {
            baggage.setTotalDimension(random.nextInt(90, 161));
        }
        if (massUnit == MassUnit.KILOGRAM) {
            baggage.setWeight(random.nextInt(8, 21));
        }
        if (massUnit == MassUnit.POUND) {
            baggage.setWeight(random.nextInt(18, 48));
        }
        return baggage;
    }

    private Hotel getHotel() {
        Hotel hotel = new Hotel();
        hotel.setId("hotel-" + hotelIdGenerator.getAndIncrement());
        hotel.setStarRating(random.nextInt(2, 6));
        hotel.setMeal(getMeal());
        hotel.setServices(getServices());
        hotel.setFacilities(getFacilities());
        hotel.setRoom(getRoom());
        return hotel;
    }

    private Meal getMeal() {
        Meal meal = new Meal();
        meal.setBoardBasis(boardBasises[random.nextInt(boardBasises.length)]);
        if (random.nextDouble() < specialRequestChance) {
            meal.setSpecialRequest(specialRequests[random.nextInt(specialRequests.length)]);
        }
        return meal;
    }

    private List<Service> getServices() {
        int n = random.nextInt(3, services.length + 1);
        return getRandomEntries(Arrays.asList(services), n);
    }

    private List<Facility> getFacilities() {
        int n = random.nextInt(4, facilities.length +1);
        return getRandomEntries(Arrays.asList(facilities), n);
    }

    private Room getRoom() {
        Room room = new Room();
        List<Bed> beds = getBeds();
        room.setBeds(beds);
        room.setSleeps(getSleepsCount(beds));
        room.setFacilities(getRoomFacilities());
        return room;
    }

    private List<Bed> getBeds() {
        int n = random.nextInt(1, 3);
        List<Bed> beds = new ArrayList<>(n);
        List<BedType> bedTypes = getRandomEntries(Arrays.asList(BedType.values()), n);
        for (BedType bedType : bedTypes) {
            Bed bed = new Bed();
            bed.setType(bedType);
            bed.setQuantity(random.nextInt(1, 2));
            beds.add(bed);
        }
        return beds;
    }

    private int getSleepsCount(List<Bed> beds) {
        return beds.stream().mapToInt(b -> b.getType().getSleeps())
                            .sum();
    }

    private List<RoomFacility> getRoomFacilities() {
        int n = random.nextInt(3, roomFacilitiesDescription.length + 1);
        List<String> roomFacilitiesDescription =
                getRandomEntries(Arrays.asList(this.roomFacilitiesDescription), n);
        List<RoomFacility> roomFacilities = new ArrayList<>(n);
        for (String description : roomFacilitiesDescription) {
            RoomFacility roomFacility = new RoomFacility();
            roomFacility.setDescription(description);
            if (random.nextDouble() < 0.3) {
                roomFacility.setQuantity(2);
            }
            roomFacilities.add(roomFacility);
        }
        return roomFacilities;
    }

    private Cost getCost() {
        Cost cost = new Cost();
        cost.setCurrency(currencies[random.nextInt(currencies.length)]);
        cost.setTravelCost(round(random.nextDouble(600, 2600)));
        cost.setHotelCost(round(random.nextDouble(700, 4300)));
        cost.setActivitiesCost(round(random.nextDouble(300, 1500)));
        return cost;
    }

    private double round(double value) {
        // Round to 2 decimal places;
        return (double) Math.round(value * 100d) / 100d;
    }

    private <T> List<T> getRandomEntries(List<T> list, int n) {
        List<T> copy = new ArrayList<>(list);
        Collections.shuffle(copy);
        return copy.subList(0, n);
    }
}